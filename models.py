from flask_login import UserMixin
from app import db

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    name = db.Column(db.String(1000))
    contact_no = db.Column(db.Unicode(255))
    created_on = db.Column(db.DateTime)
    updated_on = db.Column(db.DateTime)
    is_active = db.Column(db.Boolean)
    
    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}