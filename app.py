from flask import Flask 
from flask_sqlalchemy import SQLAlchemy 
# from flask_login import LoginManager 
from flask_cors import CORS
from flask_jwt_extended import JWTManager

db = SQLAlchemy()

# def create_app():
app = Flask(__name__)
cors = CORS(app, supports_credentials=True)
app.config['JWT_SECRET_KEY'] = "alphabetagammadelta"  # Change this!
jwt = JWTManager(app)

app.config['SECRET_KEY'] = 'thisismysecretkeydonotstealit'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://gBf5p2lLYr:sNXsG4flbU@remotemysql.com/gBf5p2lLYr'

db.init_app(app)

# login_manager = LoginManager()
# login_manager.login_view = 'auth.login'
# login_manager.init_app(app)

from models import User

# @login_manager.user_loader
# def load_user(user_id):
#     return User.query.get(int(user_id))

from auth import auth as auth_blueprint
app.register_blueprint(auth_blueprint)

from main import main as main_blueprint
app.register_blueprint(main_blueprint)
