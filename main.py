from flask import Blueprint, render_template, redirect, url_for
from flask.globals import request
# from flask_login import login_required, current_user
from models import User
from datetime import datetime as dt
from app import db
from flask_jwt_extended import create_access_token
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import jwt_required

main = Blueprint('main', __name__)

@main.route('/')
@jwt_required()
def index():
    # return render_template('index.html')
    user_id = get_jwt_identity()
    is_authenticated = False
    if User.query.get(user_id):
        is_authenticated = True
    print(user_id)
    return {"is_authenticated": is_authenticated}

@main.route('/profile')
@jwt_required()
def profile():
    users = User.query.all()
    print(users)
    ser_users = []
    for user in users:
        ser_users.append(user.as_dict())
    user_id = get_jwt_identity()
    user = User.query.get(user_id)
    # return render_template('profile.html', name=user.name, users=users)
    return {"name": user.name, "users": ser_users}

@main.route('/update')
@jwt_required()
def profileupdate():
    user_id = get_jwt_identity()
    user = User.query.get(user_id)
    # return render_template('update.html', user=user)
    return {"user": user.as_dict()}
    
    
@main.route('/update', methods=["POST"])
@jwt_required()
def profileupdatepost():
    user_id = get_jwt_identity()
    user = User.query.get(user_id)
    user.updated_on = dt.now()
    user.name = request.form.get('name')
    user.email = request.form.get('email')
    user.contact_no = request.form.get('contact')
    db.session.commit()
    # return redirect(url_for('main.profile'))
    return {"message": "Updated", "status_code": 200}

@main.route('/activate/<int:id>')
@jwt_required()
def activate(id):
    user = User.query.get(id)
    user.updated_on = dt.now()
    user.is_active = True
    db.session.commit()
    # return redirect(url_for('main.profile'))
    return {"message": "Activated", "status_code": 200}

@main.route('/deactivate/<int:id>')
@jwt_required()
def deactivate(id):
    user = User.query.get(id)
    user.updated_on = dt.now()
    user.is_active = False
    db.session.commit()
    # return redirect(url_for('main.profile'))
    return {"message": "Deactivated", "status_code": 200}